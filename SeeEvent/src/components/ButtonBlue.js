import React from 'react'
import { StyleSheet, Text, Pressable } from 'react-native'

const ButtonBlue = ({ title, onPress, moreStyle }) => {
    return (
        <Pressable style={[styles.btn, moreStyle]} onPress={onPress} android_disableSound>
            <Text style={styles.btnTxt}>{title}</Text>
        </Pressable>
    )
}

export default ButtonBlue

const styles = StyleSheet.create({
    btn: {
        backgroundColor: '#214457',
        width: '100%',
        alignItems: 'center',
        borderRadius: 10,
        borderColor: '#214457',
        borderWidth: 1,
    },
    btnTxt: {
        color: 'white',
        fontSize: 16,
        fontWeight: '700',
        paddingVertical: 12
    },
})
