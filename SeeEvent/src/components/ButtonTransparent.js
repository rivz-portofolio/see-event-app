import React from 'react'
import { StyleSheet, Text, Pressable } from 'react-native'

const ButtonTransparent = ({ onPress }) => {
    return (
        <Pressable style={styles.btn} android_disableSound onPress={onPress}>
            <Text style={styles.btnTxt}>Sign In</Text>
        </Pressable>
    )
}

export default ButtonTransparent

const styles = StyleSheet.create({
    btn: {
        backgroundColor: 'transparent',
        width: '100%',
        alignItems: 'center',
        borderRadius: 10,
        borderColor: '#214457',
        borderWidth: 1,
        marginBottom: 24,
    },
    btnTxt: {
        color: '#214457',
        fontSize: 16,
        fontWeight: '700',
        paddingVertical: 12
    },
})
