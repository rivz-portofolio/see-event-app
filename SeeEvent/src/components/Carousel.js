import React, { useRef, useState } from 'react'
import { Image, StyleSheet, Text, View, Dimensions, Pressable } from 'react-native'

import Carousel, { Pagination } from 'react-native-snap-carousel';
import carouselPhoto from '../assets/carousel.png'

const { width, height } = Dimensions.get('screen')

const CarouselHome = ({ width = 300, onPress }) => {
    const [activeSlide, setActiveSlide] = useState(0)

    const carousel = useRef(null)

    const data = [1, 2, 3]

    const renderItem = () => {
        return (
            <Pressable onPress={onPress}>
                <View style={styles.container}>
                    <Image source={carouselPhoto} style={styles.img} />
                    <View style={styles.info}>
                        <Text style={styles.tag}>Design</Text>
                        <Text style={styles.time}>SUN, OCT 24 @ 1:15 AM ICT</Text>
                        <Text style={styles.title} numberOfLines={2}>Hiting Reset: How to land a job in UX Design</Text>
                        <Text style={styles.editor}>By Adit nento</Text>
                    </View>
                </View>
            </Pressable>
        )
    }

    return (
        <View>
            <Carousel
                ref={(c) => carousel.current = c}
                data={data}
                renderItem={renderItem}
                sliderWidth={width}
                itemWidth={width}
                onSnapToItem={(index) => setActiveSlide(index) }
            />
            <Pagination
              dotsLength={data.length}
              activeDotIndex={activeSlide}
              containerStyle={{width : '40%', height: height * 0.08, alignSelf: 'center'}}
              dotStyle={{
                  width: 10,
                  height: 10,
                  borderRadius: 5,
                  marginHorizontal: 8,
                  backgroundColor: '#214457'
              }}
              inactiveDotOpacity={0.4}
              inactiveDotScale={0.6}
            />
        </View>
    )
}

export default CarouselHome

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        marginTop: 16,
        borderRadius: 8,
        overflow: 'hidden'
    },
    img: {
        width: '100%',
        height: height * 0.20,
        resizeMode: 'cover'
    },
    info: {
        padding: 10
    },
    tag: {
        backgroundColor: '#F0F2E9',
        color: '#214457',
        maxWidth: '30%',
        textAlign: 'center',
        paddingVertical: 3,
        fontSize: 14,
        marginBottom: 6,
        fontWeight: '700'
    },
    time: {
        color: '#373737',
        fontSize: 15,
        marginBottom: 3
    },
    title: {
        color: '#214457',
        fontSize: 20,
        fontWeight: '700',
        marginBottom: 3
    },
    editor: {
        color: '#999999',
        fontSize: 14
    }
})
