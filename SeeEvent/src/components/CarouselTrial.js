import React, { useRef } from 'react'
import { FlatList, StyleSheet, Text, View, Dimensions } from 'react-native'

const { width, height } = Dimensions.get('screen')
const item = [
    {
        title: 'Oke'
    },
    {
        title: 'Oke'
    },
    {
        title: 'Oke'
    },
]

const Carousel = (props) => {
    const flatRef = useRef()
    return (
            <FlatList
                data={item}
                keyExtractor={(item, index) => index}
                horizontal
                showsHorizontalScrollIndicator={false}
                pagingEnabled
                ref={ref => {
                    flatRef.current = ref
                }}
                renderItem={({item}) => (
                    <View style={{width: 300}}>
                        <View style={{
                            backgroundColor: 'red',
                            height: 300,
                            borderWidth: 1,
                        }}>
                            <Text>{item.title}</Text>
                        </View>
                    </View>
                )}
            />
    )
}

export default Carousel

const styles = StyleSheet.create({})
