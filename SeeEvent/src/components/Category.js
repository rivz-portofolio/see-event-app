import React from 'react'
import { FlatList, StyleSheet, Text, View, Dimensions, Pressable } from 'react-native'

const { width, height } = Dimensions.get('screen')

const Category = ({ onPress }) => {
    const data = ['Photography', 'Design', 'Development', 'Marketing', 'Business', 'Lifestyle', 'Music']
    return (
        <FlatList
            data={data}
            keyExtractor={(item, index) => index}
            horizontal
            showsHorizontalScrollIndicator={false}
            renderItem={({item}) => (
                <Pressable style={styles.card} onPress={onPress}>
                    <Text style={styles.txt}>{item}</Text>
                </Pressable>
            )}
        />
    )
}

export default Category

const styles = StyleSheet.create({
    card: {
        paddingVertical: 10,
        paddingHorizontal: 20,
        backgroundColor: 'white',
        marginRight: 7,
        borderRadius: 100,
        borderWidth: 2,
        borderColor: '#214457'
    },
    txt: {
        color: '#214457',
        fontSize: width * 0.05,
        fontWeight: '700'
    }
})
