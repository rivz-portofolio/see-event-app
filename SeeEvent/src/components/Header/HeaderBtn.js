import React from 'react'
import { StyleSheet, Text, View, Dimensions, Pressable } from 'react-native'
import IonIcon from 'react-native-vector-icons/Ionicons'

const ww = Dimensions.get('window').width
const wh = Dimensions.get('window').height

const HeaderBtn = ({title, onPress}) => {
    return (
        <View style={styles.container}>
            <Pressable onPress={onPress} android_disableSound >
                <IonIcon name='arrow-back-outline' size={30} color='#fcfcfc' />
            </Pressable>
            <Text style={styles.title}>{title}</Text>
        </View>
    )
}

export default HeaderBtn

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#214457',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 16,
        paddingVertical: 12
    },
    title: {
        color: 'white',
        marginLeft: 10,
        fontSize: 20,
        fontWeight: '700'
    }
})
