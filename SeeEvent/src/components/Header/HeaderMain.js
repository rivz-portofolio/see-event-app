import React from 'react'
import { StyleSheet, Text, View, Dimensions, Pressable, Image } from 'react-native'
import headerlogo from '../../assets/header-logo.png'

const ww = Dimensions.get('window').width
const wh = Dimensions.get('window').height

const HeaderMain = () => {
    return (
        <View style={styles.container}>
            <Image source={headerlogo} style={{height: 32}} />
        </View>
    )
}

export default HeaderMain

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#214457',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 16,
        paddingVertical: 12
    },
    title: {
        color: 'white',
        marginLeft: 10,
        fontSize: 20,
        fontWeight: '700'
    }
})
