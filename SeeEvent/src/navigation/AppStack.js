import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';

import MainNavigator from './MainNavigator';
import SplashScreen from '../screens/SplashScreen';
import Login from '../screens/Auth/Login';
import Register from '../screens/Auth/Register';

const Stack = createStackNavigator();

const AppStack = () => {
    return (
        <Stack.Navigator screenOptions={{headerShown: false}}>
            <Stack.Screen name="Splash" component={SplashScreen} />
            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="Register" component={Register} />
            <Stack.Screen name="Main" component={MainNavigator} />
        </Stack.Navigator>
    )
}

export default AppStack
