import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';

import Home from '../screens/Home/Home';
import EventDetail from '../screens/Events/EventDetail'

const Stack = createStackNavigator();

const HomeStack = () => {
    return (
        <Stack.Navigator screenOptions={{headerShown: false}}>
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="Event Detail" component={EventDetail} />
        </Stack.Navigator>
    )
}

export default HomeStack
