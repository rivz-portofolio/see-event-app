import React from 'react'
import { Dimensions } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import IonIcon from 'react-native-vector-icons/Ionicons'

import Explore from '../screens/Explore';
import Profile from '../screens/Profile/Profile';
import HomeStack from './HomeStack';

const ww = Dimensions.get('window').width
const wh = Dimensions.get('window').height

const Tab = createBottomTabNavigator();

const MainNavigator = () => {
    return (
        <Tab.Navigator screenOptions={({route}) => ({
            headerShown: false,
            tabBarActiveTintColor: '#214457',
            tabBarInactiveTintColor: '#999',
            tabBarActiveBackgroundColor: '#ECEEEF',
            tabBarStyle: {
                borderTopWidth: 2,
                borderColor: 'rgba(55, 55, 55, 0.3)',
                height: wh * 0.072,
            },
            tabBarLabelStyle: {
                fontSize: ww * 0.03
            },
            tabBarIcon: ({size, color, focused}) => {
                let iconName;

                if(route.name == 'Home Main') {
                    iconName = focused ? 'home' : 'home-outline'
                } else if(route.name == 'Explore') {
                    iconName = focused ? 'search' : 'search-outline'
                } else if(route.name == 'Profile') {
                    iconName = focused ? 'person' : 'person-outline'
                }

                return <IonIcon name={iconName} size={ww * 0.07} color={color} />
            }
        })}>
            <Tab.Screen name="Home Main" component={HomeStack} />
            <Tab.Screen name="Explore" component={Explore} />
            <Tab.Screen name="Profile" component={Profile} />
        </Tab.Navigator>
    )
}

export default MainNavigator
// 214457