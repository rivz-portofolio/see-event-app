const initialState = {
    isLoggedin: false,
    isLoading: false
};

const auth = (state = initialState, action) => {
    switch(action.type) {
        case 'LOADING':
            return {
                ...state,
                isLoading: action.value
            }
        case 'LOGIN':
            return {
                ...state,
                isLoggedin: action.value
            }
        default:
            return state
    }
}

export default auth;