const initialState = {
    firstName: 'Pratur',
    lastName: 'Anahata',
    email: 'praturanahata@gmail.com',
    password: '123456'
};

const profile = (state = initialState, action) => {
    switch(action.type) {
        case 'SAVE_PROFILE':
            return {
                ...state,
                firstName: action.data.firstName ? action.data.firstName : 'Pratur',
                lastName: action.data.lastName ? action.data.lastName :'Anahata',
                email: action.data.email,
                password: action.data.password
            }
        case 'DELETE_PROFILE':
            return {
                ...state,
                firstName: null,
                lastName: null,
                email: null,
                password: null
            }
        default:
            return state
    }
}

export default profile;