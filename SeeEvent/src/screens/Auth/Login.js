import React, { useState } from 'react'
import { ScrollView, StyleSheet, Text, TextInput, View, Dimensions, Pressable } from 'react-native'
import IonIcon from 'react-native-vector-icons/Ionicons'
import { useDispatch } from 'react-redux'

// Components
import Header from '../../components/Header/HeaderBtn'

const ww = Dimensions.get('window').width
const wh = Dimensions.get('window').height

const Login = ({ navigation }) => {
    const [visible, setVisible] = useState(false)
    const [email, setEmail] = useState()
    const [password, setPassword] = useState()
    const dispatch = useDispatch()

    const handleLogin = () => {
        if(!email) {
            alert('Please fill in the Email')
        } else if(!password) {
            alert('Please fill in the Password')
        } else {
            const data = {
                email,
                password
            };

            dispatch({type: 'SAVE_PROFILE', data})
            dispatch({type: 'LOGIN', value: true})
            navigation.navigate('Main')
        }
    }

    return (
        <View style={styles.container}>
            <Header title='Sign In' onPress={() => navigation.goBack()} />
            <ScrollView contentContainerStyle={styles.content}>
                <Text style={styles.title}>Welcome Back !</Text>
                <TextInput
                    placeholder='Email'
                    placeholderTextColor='grey'
                    style={styles.txtInput}
                    value={email}
                    onChangeText={text => setEmail(text)}
                />
                <View style={styles.txtInputContainer}>
                    <TextInput
                        placeholder='Password'
                        placeholderTextColor='grey'
                        style={styles.txtInputIcn}
                        secureTextEntry={!visible}
                        value={password}
                        onChangeText={text => setPassword(text)}
                    />
                    <Pressable onPress={() => setVisible(!visible)} android_disableSound>
                        <IonIcon name={visible ? 'eye-outline' : 'eye-off-outline'} size={30} color='grey' />
                    </Pressable>
                </View>
                <Pressable style={styles.btn} android_disableSound onPress={handleLogin}>
                    <Text style={styles.btnTxt}>Sign In</Text>
                </Pressable>
                <Pressable style={styles.btnFrgt} android_disableSound>
                    <Text style={styles.btnTxtFrgt}>Forgot Password ?</Text>
                </Pressable>
            </ScrollView>
        </View>
    )
}

export default Login

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ECEEEF',
    },
    content: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 16,
        paddingTop: wh * 0.2,
        paddingBottom: wh * 0.05,
    },
    title: {
        fontSize: 40,
        color: '#214457',
        fontWeight: '700',
        marginBottom: 32
    },
    txtInput: {
        borderWidth: 1,
        width: '100%',
        marginBottom: 24,
        borderRadius: 10,
        borderColor: 'grey',
        paddingHorizontal: 16,
        fontSize: 20,
        backgroundColor: 'white'
    },
    txtInputIcn: {
        paddingHorizontal: 16,
        fontSize: 20,
        flex: 1
    },
    txtInputContainer: {
        borderColor: 'grey',
        paddingRight: 16,
        borderWidth: 1,
        width: '100%',
        marginBottom: 24,
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    btn: {
        backgroundColor: '#214457',
        width: '100%',
        alignItems: 'center',
        borderRadius: 10,
        marginVertical: 14,
    },
    btnTxt: {
        color: 'white',
        fontSize: 16,
        fontWeight: '700',
        paddingVertical: 12
    },
    btnFrgt: {
        
    },
    btnTxtFrgt: {
        color: '#3E89AE',
        fontSize: 16,
        fontWeight: '700',
    }
})
