import React from 'react'
import { ScrollView, StyleSheet, Text, View, Dimensions, Image, Pressable, TextInput } from 'react-native'
import IonIcon from 'react-native-vector-icons/Ionicons'

import HeaderBtn from '../../components/Header/HeaderBtn'
import eventPhoto from '../../assets/eventPhoto.png'
import avatar from '../../assets/avatar.png'

const ww = Dimensions.get('window').width
const wh = Dimensions.get('window').height

const line = '\n'

const EventDetail = ({ navigation }) => {
    const data = [
        {
            name: 'Elon Musk',
            comment: 'Hi, do you have a youtube channel that I can subscribe?'
        },
        {
            name: 'Elon Musk',
            comment: 'Hi, do you have a youtube channel that I can subscribe?'
        },
        {
            name: 'Elon Musk',
            comment: 'Hi, do you have a youtube channel that I can subscribe?'
        },
    ]

    return (
        <View style={{flex: 1, backgroundColor: '#ECEEEF'}}>
            <HeaderBtn title='Event Detail' onPress={() => navigation.goBack()} />
            <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.content}>
                <Text style={styles.title} numberOfLines={3}>ESL Game: English on Your Feet!</Text>
                <View style={styles.dateContainer}>
                    <IonIcon name='calendar-outline' size={ww * 0.05} color='black' />
                    <Text style={styles.date}>SUN, OCT 24 @ 1:15 AM ICT</Text>
                    <Text style={styles.tag}>Business</Text>
                </View>
                <Image source={eventPhoto} style={styles.img} />
                <View style={styles.profileContainer}>
                    <Image source={avatar} style={styles.profileImg} />
                    <View style={styles.profileInfo}>
                        <Text style={styles.profileCreator}>Created by</Text>
                        <Text style={styles.profileName}>Pratur Anahata Pratama</Text>
                    </View>
                </View>
                <Text style={styles.detailTitle}>Details</Text>
                <Text style={styles.detailText}>
                    Welcome to the Parlor! Let's play English on Your Feet!™{line}
                    {line}
                    GET FEEDBACK{line}
                    GAIN CONFIDENCE{line}
                    LAYER UP YOUR ENGLISH!™{line}
                    {line}
                    Everyone will have a chance to speak to the "audience" and receive feedback from the audience and our coaches. You don't need to prepare anything--just prepare to have fun and try!{line}
                    {line}
                    Relax. Layer Up your English!™
                </Text>
                <Text style={styles.detailTitle}>Comments</Text>
                {
                    data.map((item, index) => (
                        <View key={index} style={{marginBottom: 16}}>
                            <View style={[styles.profileContainer, {marginBottom: 10}]}>
                                <Image source={avatar} style={styles.profileImg} />
                                <View style={styles.profileInfo}>
                                    <Text style={styles.profileName}>{item.name}</Text>
                                    <Text style={styles.profileCreator}>4 Days Ago</Text>
                                </View>
                            </View>
                            <Text style={styles.detailText}>{item.comment}</Text>
                        </View>
                    ))
                }
                <View style={[styles.profileContainer, {marginBottom: 16}]}>
                    <Image source={avatar} style={styles.profileImg} />
                    <Text style={styles.profileName}>John Doe</Text>
                </View>
                <TextInput
                    placeholder='Insert Comments...'
                    placeholderTextColor='grey'
                    multiline
                    style={styles.commentInput}
                />
                <Pressable style={styles.btn} android_disableSound>
                    <IonIcon name='chatbubble-outline' size={ww * 0.05} color='white' />
                    <Text style={styles.btnTxt}>Submit</Text>
                </Pressable>
            </View>
            </ScrollView>
            <View style={{
                backgroundColor: 'white',
                flexDirection: 'row',
                justifyContent: 'space-between',
                padding: 16,
            }}>
                <Pressable style={[styles.btnFooter, styles.btnFooterWhite]} android_disableSound>
                    <IonIcon name='share-social-outline' size={ww * 0.05} color='#214457' />
                    <Text style={[styles.btnTxt, styles.btnTxtWhite]}>Share</Text>
                </Pressable>
                <Pressable style={styles.btnFooter} android_disableSound>
                    <IonIcon name='bookmark-outline' size={ww * 0.05} color='white' />
                    <Text style={styles.btnTxt}>Save</Text>
                </Pressable>

            </View>
        </View>
    )
}

export default EventDetail

const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingHorizontal: 16,
        paddingVertical: 20
    },
    title: {
        color: 'black',
        fontSize: ww * 0.07,
        fontWeight: 'bold',
        marginBottom: 10
    },
    dateContainer: {
        flexDirection: 'row'
    },
    date: {
        color: 'black',
        fontSize: ww * 0.04,
        marginBottom: 10,
        marginLeft: 10
    },
    tag: {
        backgroundColor: '#F0F2E9',
        paddingVertical: 3,
        paddingHorizontal: 14,
        marginLeft: 'auto',
        color: '#214457',
        textAlign: 'center',
        fontSize: 14,
        fontWeight: '700'
    },
    img: {
        width: '100%',
        maxHeight: wh * 0.3,
        borderRadius: 8,
        marginVertical: 16
    },
    profileContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    profileImg: {
        width: ww * 0.14,
        aspectRatio: 1,
        borderRadius: (ww * 0.14) / 2,
        marginRight: 12
    },
    profileCreator: {
        color: '#373737',
        fontSize: ww * 0.035,
    },
    profileName: {
        color: '#373737',
        fontSize: ww * 0.045,
        fontWeight: '700'
    },
    detailTitle: {
        color: 'black',
        fontSize: ww * 0.06,
        fontWeight: '600',
        marginVertical: 16
    },
    detailText: {
        color: 'black',
        fontSize: ww * 0.04,
    },
    commentInput: {
        backgroundColor: 'white',
        borderRadius: 8,
        borderColor: 'grey',
        borderWidth: 1,
        paddingHorizontal: 10,
        maxHeight: wh * 0.25,
        color: 'black',
        fontSize: ww * 0.04,
    },
    btn: {
        backgroundColor: '#214457',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        marginTop: 16,
        paddingVertical: 12
    },
    btnTxt: {
        color: 'white',
        fontSize: ww * 0.045,
        fontWeight: '700',
        marginLeft: 10
    },
    btnFooter: {
        backgroundColor: '#214457',
        width: '47%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        paddingVertical: 12
    },
    btnFooterWhite: {
        backgroundColor: 'white',
        borderColor: '#214457',
        borderWidth: 2
    },
    btnTxtWhite: {
        color: '#214457',
    },
})
