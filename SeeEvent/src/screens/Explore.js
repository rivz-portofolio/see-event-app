import React, { useState } from 'react'
import { StyleSheet, Text, TextInput, View, Pressable, Dimensions, FlatList, Image, ScrollView } from 'react-native'
import IonIcon from 'react-native-vector-icons/Ionicons'

import cardPhoto from '../assets/carousel.png'
import noResult from '../assets/noResult.png'
import ModalSort from '../components/ModalSort'

const ww = Dimensions.get('window').width
const wh = Dimensions.get('window').height

const Explore = () => {
    const [isBookmark, setIsBookmark] = useState(false)
    const [isEmpty, setIsEmpty] = useState(false)
    const [viewModal, setViewModal] = useState(false)
    const [sortByTitle, setSortByTitle] = useState()
    const [sortBy, setSortBy] = useState()
    const [sort, setSort] = useState()
    const [date, setDate] = useState()
    const [category, setCategory] = useState()
    const [search, setSearch] = useState()

    const data = ['Sort by', 'Upcoming', 'Category']
    const dataContent = [1, 2, 3]

    const CardContainer = ({item}) => {
        return (
            <View style={styles.cardContainer}>
                <View style={styles.cardUpper}>
                    <View style={styles.upperLeft}>
                        <Text style={styles.time}>SUN, OCT 24 @ 1:15 AM ICT</Text>
                        <Text style={styles.title} numberOfLines={2}>Hiting Reset: How to land a job in UX Design</Text>
                        <Text style={styles.editor}>By Adit nento</Text>
                    </View>
                    <Image source={cardPhoto} style={styles.img} />
                </View>
                <View style={styles.cardLower}>
                    <Text style={styles.tag}>Design</Text>
                    <View style={styles.icnTag}>
                        <IonIcon name={'share-social-outline'} size={ww * 0.06} color='grey' />
                        <Pressable onPress={() => setIsBookmark(!isBookmark)} android_disableSound>
                            <IonIcon name={isBookmark ? 'bookmark' : 'bookmark-outline'} size={ww * 0.06} color='grey' />
                        </Pressable>
                    </View>
                </View>
            </View>
        )
    }

    if(isEmpty) {
        return (
            <View style={styles.container}>
                 <View style={styles.header}>
                    <View style={styles.inputContainer}>
                        <TextInput placeholder='Search here...' style={styles.txtInput} onChangeText={text => setSearch(text)} />
                        <Pressable onPress={() => alert('Search')} android_disableSound>
                            <IonIcon name={'search-outline'} size={ww * 0.07} color='grey' />
                        </Pressable>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <Pressable style={styles.catContainer} onPress={() => {
                            setViewModal(!viewModal)
                            setSortByTitle('Sort by')
                        }}>
                            <Text style={styles.catText}>{sort ? sort : 'Sort by'}</Text>
                            <IonIcon name={'chevron-down-outline'} size={ww * 0.04} color='grey' />
                        </Pressable>
                        <Pressable style={styles.catContainer} onPress={() => {
                            setViewModal(!viewModal)
                            setSortByTitle('Upcoming')
                        }}>
                            <Text style={styles.catText}>{date ? date : 'Upcoming'}</Text>
                            <IonIcon name={'chevron-down-outline'} size={ww * 0.04} color='grey' />
                        </Pressable>
                        <Pressable style={styles.catContainer} onPress={() => {
                            setViewModal(!viewModal)
                            setSortByTitle('Category')
                        }}>
                            <Text style={styles.catText}>{category ? category : 'Category'}</Text>
                            <IonIcon name={'chevron-down-outline'} size={ww * 0.04} color='grey' />
                        </Pressable>
                    </View>
                </View>
                <View style={styles.content}>
                    <Text style={styles.titleResult}>Showing 0 Results for "{search}"</Text>
                    <Image source={noResult} style={{maxHeight: wh * 0.6, width: '100%', resizeMode: 'contain', marginVertical: 32}} />
                    <Text style={{fontSize: ww * 0.045, textAlign: 'center', color: 'black'}}>Sorry, we couldn't find any events from this search.</Text>
                </View>
                
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.inputContainer}>
                    <TextInput placeholder='Search here...' style={styles.txtInput} onChangeText={text => setSearch(text)} />
                    <Pressable onPress={() => alert('Search')} android_disableSound>
                        <IonIcon name={'search-outline'} size={ww * 0.07} color='grey' />
                    </Pressable>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <Pressable style={styles.catContainer} onPress={() => {
                        setViewModal(!viewModal)
                        setSortByTitle('Sort by')
                    }}>
                        <Text style={styles.catText}>{sort ? sort : 'Sort by'}</Text>
                        <IonIcon name={'chevron-down-outline'} size={ww * 0.04} color='grey' />
                    </Pressable>
                    <Pressable style={styles.catContainer} onPress={() => {
                        setViewModal(!viewModal)
                        setSortByTitle('Upcoming')
                    }}>
                        <Text style={styles.catText}>{date ? date : 'Upcoming'}</Text>
                        <IonIcon name={'chevron-down-outline'} size={ww * 0.04} color='grey' />
                    </Pressable>
                    <Pressable style={styles.catContainer} onPress={() => {
                        setViewModal(!viewModal)
                        setSortByTitle('Category')
                    }}>
                        <Text style={styles.catText}>{category ? category : 'Category'}</Text>
                        <IonIcon name={'chevron-down-outline'} size={ww * 0.04} color='grey' />
                    </Pressable>
                </View>
            </View>
            <View style={styles.content}>
                <Text style={styles.titleResult}>Showing 20 Results for "{search}"</Text>
                <FlatList
                    data={dataContent}
                    keyExtractor={(item, index) => index}
                    showsVerticalScrollIndicator={false}
                    renderItem={CardContainer}
                />
            </View>
            {
                viewModal &&
                <ModalSort
                    viewModal={viewModal}
                    modalType={sortByTitle}
                    setSortBy={setSortBy}
                    setViewModal={() => setViewModal(!viewModal)}
                    setSort={setSort}
                    setDate={setDate}
                    setCategory={setCategory}
                />
            }
        </View>
    )
}

export default Explore

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ECEEEF',
    },
    content: {
        paddingHorizontal: 16,
        paddingTop: 20,
        flex: 1,
    },
    header: {
        backgroundColor: '#214457',
        paddingHorizontal: 16,
        paddingVertical: 10,
    },
    inputContainer: {
        flexDirection: 'row',
        backgroundColor: 'white',
        borderRadius: 8,
        paddingHorizontal: 10,
        alignItems: 'center',
        marginBottom: 10
    },
    txtInput: {
        flex: 1,
        marginRight: 5,
        paddingVertical: 6,
        fontSize: ww * 0.05
    },
    catContainer: {
        backgroundColor: '#F0F2E9',
        flexDirection: 'row',
        marginRight: 10,
        borderRadius: 8,
        paddingHorizontal: 8,
        paddingVertical: 2,
        alignItems: 'center'
    },
    cardContainer: {
        marginBottom: 20,
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderColor: 'grey'
    },
    catText: {
        color: '#214457',
        marginRight: 6,
        fontSize: ww * 0.04
    },
    titleResult: {
        fontWeight: '700',
        color: 'black',
        fontSize: ww * 0.05,
        marginBottom: 16
    },
    cardUpper: {
        flexDirection: 'row',
        marginBottom: 10
    },
    upperLeft: {
        flex: 1,
        marginRight: 8,
    },
    img: {
        width: ww * 0.4,
        height: ww * 0.25,
        borderRadius: 8
    },
    time: {
        color: '#373737',
        fontSize: ww * 0.035,
        marginBottom: 3
    },
    title: {
        color: '#214457',
        fontSize: ww * 0.04,
        fontWeight: '700',
        marginBottom: 3
    },
    editor: {
        color: '#999999',
        fontSize: ww * 0.03
    },
    cardLower: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    tag: {
        backgroundColor: '#F0F2E9',
        color: '#214457',
        maxWidth: '30%',
        textAlign: 'center',
        paddingVertical: 3,
        paddingHorizontal: 14,
        fontSize: 14,
        marginBottom: 6,
        fontWeight: '700'
    },
    icnTag: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '15%'
    }
})
