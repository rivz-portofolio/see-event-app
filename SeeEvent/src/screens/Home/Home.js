import React, { useState } from 'react'
import { Pressable, ScrollView, StyleSheet, Text, View } from 'react-native'

import HeaderMain from '../../components/Header/HeaderMain'
import Carousel from '../../components/Carousel'
import Category from '../../components/Category'

const Home = ({ navigation }) => {
    const [carWidth, setCarWidth] = useState()

    return (
        <View style={styles.container}>
            <HeaderMain />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.content}>
                        <View style={styles.section} onLayout={event => setCarWidth(event.nativeEvent.layout.width)}>
                            <View style={styles.sectionHeader}>
                                <Text style={styles.sectionTitle}>Starting Soon</Text>
                                <Pressable style={styles.sectionBtn}>
                                    <Text style={styles.sectionBtnTxt}>See All</Text>
                                </Pressable>
                            </View>
                            <Carousel width={carWidth} onPress={() => navigation.navigate('Event Detail')} />
                        </View>

                        <View style={styles.explore}>
                            <Text style={[styles.sectionTitle, {marginBottom: 10}]}>Explore by Category</Text>
                            <Category onPress={() => navigation.navigate('Explore')} />
                        </View>

                        <View style={styles.section} onLayout={event => setCarWidth(event.nativeEvent.layout.width)}>
                            <View style={styles.sectionHeader}>
                                <Text style={styles.sectionTitle}>Design Events</Text>
                                <Pressable style={styles.sectionBtn}>
                                    <Text style={styles.sectionBtnTxt}>See All</Text>
                                </Pressable>
                            </View>
                            <Carousel width={carWidth} onPress={() => navigation.navigate('Event Detail')} />
                        </View>

                </View>
            </ScrollView>
        </View>
    )
}

export default Home

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ECEEEF'
    },
    content: {
        flex: 1,
        paddingHorizontal: 16,
        paddingTop: 20
    },
    section: {
        
    },
    sectionHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    sectionTitle: {
        fontSize: 20,
        fontWeight: '700',
        color: 'black'
    },
    sectionBtnTxt: {
        fontSize: 14,
        fontWeight: '700',
        color: '#3E89AE'
    },
    explore: {
        marginBottom: 30
    }
})
