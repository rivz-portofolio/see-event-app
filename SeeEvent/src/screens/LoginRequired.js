import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'

import ButtonBlue from '../components/ButtonBlue'
import ButtonTransparent from '../components/ButtonTransparent'
import HeaderMain from '../components/Header/HeaderMain'

import loginIcn from '../assets/login-req.png'

const LoginRequired = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <HeaderMain />
            <View style={styles.content}>
                <Text style={styles.title}>Sign In or Sign Up to Continue</Text>
                <Image source={loginIcn} style={styles.img} />
                <ButtonBlue title='Sign Up' moreStyle={{marginBottom: 15}} onPress={() => navigation.navigate('Register')} />
                <ButtonTransparent onPress={() => navigation.navigate('Login')} />
            </View>
        </View>
    )
}

export default LoginRequired

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ECEEEF',
    },
    content: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 24
    },
    title: {
        fontWeight: '700',
        fontSize: 24,
        color: '#214457',
        marginBottom: 32
    },
    img: {
        marginBottom: 32,
        height: 240
    }
})
