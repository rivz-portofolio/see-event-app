import React from 'react'
import { Image, StyleSheet, Text, View, Pressable } from 'react-native'
import IonIcon from 'react-native-vector-icons/Ionicons'

// Screens
import Header from '../../components/Header/Header'
import LoginRequired from '../LoginRequired'

import avatar from '../../assets/avatar.png'
import ticket from '../../assets/Ticket-Square.png'

import { useDispatch, useSelector } from 'react-redux'
import { NavigationContainer } from '@react-navigation/native'

const Profile = ({ navigation }) => {
    const profile = useSelector(state => state.profile)
    const auth = useSelector(state => state.auth.isLoggedin)
    const dispatch = useDispatch()

    const handleLogout = () => {
        navigation.navigate('Splash')
        dispatch({type: 'DELETE_PROFILE'})
        dispatch({type: 'LOGIN', value: false})
    }

    if(!auth) {
        return <LoginRequired navigation={navigation} />
    }

    return (
        <View style={styles.container}>
            <Header title='Profile' />
            <View style={styles.content}>
                <View style={styles.info}>
                    <Image source={avatar} style={styles.img} />
                    <Text style={styles.name}>{profile.firstName} {profile.lastName}</Text>
                    <Text style={styles.email}>{profile.email}</Text>
                </View>
                <Pressable style={styles.menus} onPress={() => alert('Pressed')} android_disableSound>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <IonIcon name={'person-outline'} size={24} color='#214457' />
                        <Text style={styles.menuTxt}>Edit Profile</Text>
                    </View>
                    <IonIcon name={'chevron-forward-outline'} size={24} color='#214457' />
                </Pressable>
                <Pressable style={styles.menus} onPress={() => alert('Pressed')} android_disableSound style={styles.menus}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <IonIcon name={'person-outline'} size={24} color='#214457' />
                        <Text style={styles.menuTxt}>Edit Password</Text>
                    </View>
                    <IonIcon name={'chevron-forward-outline'} size={24} color='#214457' />
                </Pressable>
                <Pressable style={styles.menus} onPress={() => alert('Pressed')} android_disableSound style={styles.menus}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Image source={ticket} style={{width: 24, height: 24}} />
                        <Text style={styles.menuTxt}>Saved Events</Text>
                    </View>
                    <IonIcon name={'chevron-forward-outline'} size={24} color='#214457' />
                </Pressable>
                <Pressable style={styles.menus} onPress={handleLogout} android_disableSound style={styles.menus}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <IonIcon name={'log-out-outline'} size={24} color='#214457' />
                        <Text style={styles.menuTxt}>Log out</Text>
                    </View>
                </Pressable>
                <Text style={styles.version}>Version v1.0</Text>
            </View>
        </View>
    )
}

export default Profile

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ECEEEF',
    },
    content: {
        paddingTop: 30,
        paddingHorizontal: 16,
        flex: 1
    },
    info: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 16,
        backgroundColor: 'white',
        borderRadius: 10,
        marginBottom: 24
    },
    img: {
        marginBottom: 16,
        width: 120,
        height: 120,
        borderRadius: 120 / 2
    },
    name: {
        fontSize: 20,
        fontWeight: '700',
        color: '#373737',
        marginBottom: 8
    },
    email: {
        fontSize: 16,
        color: '#373737'
    },
    menus: {
        borderRadius: 10,
        padding: 20,
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 16
    },
    menuTxt: {
        marginLeft: 16,
        fontSize: 16,
        fontWeight: '700',
        color: '#214457'
    },
    version: {
        fontSize: 12,
        fontWeight: '700',
        color: '#999999',
        alignSelf: 'center',
        marginTop: 20
    }
})
