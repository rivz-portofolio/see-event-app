import React from 'react'
import { Image, StyleSheet, Text, View, Pressable, TextInput, ScrollView } from 'react-native'
import IonIcon from 'react-native-vector-icons/Ionicons'

// Screens
import Header from '../../components/HeaderBtn'
import avatar from '../assets/avatar.png'

const ProfileEdit = () => {
    return (
        <View style={styles.container}>
            <Header title='Profile' />
            <View style={styles.content}>
                <ScrollView>
                    <View style={styles.info}>
                        <Image source={avatar} style={styles.img} />
                        <Pressable style={styles.btn} android_disableSound>
                            <IonIcon name='cloud-upload-outline' size={24} color='#214457' />
                            <Text style={styles.btnTxt}>Upload new picture</Text>
                        </Pressable>
                    </View>
                    <TextInput
                        placeholder='First Name'
                        placeholderTextColor='grey'
                        style={styles.txtInput}
                    />
                    <TextInput
                        placeholder='Last Name'
                        placeholderTextColor='grey'
                        style={styles.txtInput}
                    />
                    <TextInput
                        placeholder='Email'
                        placeholderTextColor='grey'
                        style={styles.txtInput}
                    />
                </ScrollView>
                <Pressable style={[styles.btn, {backgroundColor: '#214457'}]} android_disableSound>
                    <Text style={[styles.btnTxt, {color: 'white'}]}>Sign In</Text>
                </Pressable>
            </View>
        </View>
    )
}

export default ProfileEdit

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ECEEEF',
    },
    content: {
        paddingTop: 30,
        paddingHorizontal: 16,
        flex: 1
    },
    info: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 16,
        backgroundColor: 'white',
        borderRadius: 10,
        marginBottom: 24
    },
    img: {
        marginBottom: 16,
        width: 120,
        height: 120,
        borderRadius: 120 / 2
    },
    btn: {
        backgroundColor: 'transparent',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#214457',
        marginTop: 5,
        marginBottom: 14,
        flexDirection: 'row',
        paddingVertical: 10
    },
    btnTxt: {
        color: '#214457',
        fontSize: 16,
        fontWeight: '700',
        marginLeft: 8
    },
    txtInput: {
        borderWidth: 1,
        width: '100%',
        marginBottom: 24,
        borderRadius: 10,
        borderColor: 'grey',
        paddingHorizontal: 16,
        fontSize: 20,
        backgroundColor: 'white'
    },
})
