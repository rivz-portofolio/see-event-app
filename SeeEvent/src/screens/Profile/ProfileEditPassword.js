import React, { useState } from 'react'
import { Image, StyleSheet, Text, View, Pressable, TextInput, ScrollView } from 'react-native'
import IonIcon from 'react-native-vector-icons/Ionicons'

// Screens
import Header from '../../components/HeaderBtn'
import avatar from '../assets/avatar.png'

const ProfileEditPassword = () => {
    const [visible, setVisible] = useState(false)
    const [visibleNew, setVisibleNew] = useState(false)
    const [visibleConfirm, setVisibleConfirm] = useState(false)

    return (
        <View style={styles.container}>
            <Header title='Edit Password' />
            <View style={styles.content}>
                <ScrollView>
                    <View style={styles.txtInputContainer}>
                        <TextInput
                            placeholder='Old Password'
                            placeholderTextColor='grey'
                            style={styles.txtInputIcn}
                            secureTextEntry={!visible}
                        />
                        <Pressable onPress={() => setVisible(!visible)} android_disableSound>
                            <IonIcon name={visible ? 'eye-outline' : 'eye-off-outline'} size={30} color='grey' />
                        </Pressable>
                    </View>
                    <View style={styles.txtInputContainer}>
                        <TextInput
                            placeholder='New Password'
                            placeholderTextColor='grey'
                            style={styles.txtInputIcn}
                            secureTextEntry={!visibleNew}
                        />
                        <Pressable onPress={() => setVisibleNew(!visibleNew)} android_disableSound>
                            <IonIcon name={visibleNew ? 'eye-outline' : 'eye-off-outline'} size={30} color='grey' />
                        </Pressable>
                    </View>
                    <View style={styles.txtInputContainer}>
                        <TextInput
                            placeholder='Confirm Password'
                            placeholderTextColor='grey'
                            style={styles.txtInputIcn}
                            secureTextEntry={!visibleConfirm}
                        />
                        <Pressable onPress={() => setVisibleConfirm(!visibleConfirm)} android_disableSound>
                            <IonIcon name={visibleConfirm ? 'eye-outline' : 'eye-off-outline'} size={30} color='grey' />
                        </Pressable>
                    </View>
                </ScrollView>
                <Pressable style={[styles.btn, {backgroundColor: '#214457'}]} android_disableSound>
                    <Text style={[styles.btnTxt, {color: 'white'}]}>Change Password</Text>
                </Pressable>
            </View>
        </View>
    )
}

export default ProfileEditPassword

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ECEEEF',
    },
    content: {
        paddingTop: 30,
        paddingHorizontal: 16,
        flex: 1
    },
    info: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 16,
        backgroundColor: 'white',
        borderRadius: 10,
        marginBottom: 24
    },
    img: {
        marginBottom: 16,
        width: 120,
        height: 120,
        borderRadius: 120 / 2
    },
    btn: {
        backgroundColor: 'transparent',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#214457',
        marginTop: 5,
        marginBottom: 14,
        flexDirection: 'row',
        paddingVertical: 10
    },
    btnTxt: {
        color: '#214457',
        fontSize: 16,
        fontWeight: '700',
        marginLeft: 8
    },
    txtInputIcn: {
        paddingHorizontal: 16,
        fontSize: 20,
        flex: 1
    },
    txtInputContainer: {
        borderColor: 'grey',
        paddingRight: 16,
        borderWidth: 1,
        width: '100%',
        marginBottom: 24,
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
})
