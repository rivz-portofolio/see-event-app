import React from 'react'
import { StyleSheet, Text, View, Pressable,Dimensions, Image } from 'react-native'

// Assets
import icn from '../assets/icn1.png'
import logo from '../assets/SeeEvent-Logo.png'

const ww = Dimensions.get('window').width
const wh = Dimensions.get('window').height

const SplashScreen = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <Image source={icn} />
            <View style={styles.titleContainer}>
                <Image source={logo} />
                <Text style={styles.title}>SeeEvent</Text>
            </View>
            <Pressable style={styles.btn} onPress={() => navigation.navigate('Register')} android_disableSound>
                <Text style={styles.btnTxt}>Sign Up</Text>
            </Pressable>
            <Pressable style={[styles.btn, {backgroundColor: 'transparent'}]} onPress={() => navigation.navigate('Login')} android_disableSound>
                <Text style={[styles.btnTxt, {color: '#214457'}]}>Sign In</Text>
            </Pressable>
            <Pressable style={styles.btnGuest} onPress={() => navigation.navigate('Main')} android_disableSound>
                <Text style={styles.btnTxtGuest}>Continue as guest</Text>
            </Pressable>
            <Text style={styles.version}>Version v1.0</Text>
        </View>
    )
}

export default SplashScreen

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 16,
        backgroundColor: '#ECEEEF',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    titleContainer: {
        marginTop: 80,
        marginBottom: 24,
        flexDirection: 'row'
    },
    title: {
        fontSize: 32,
        fontStyle: 'italic',
        fontWeight: '700',
        color: '#214457'
    },
    btn: {
        backgroundColor: '#214457',
        width: '100%',
        alignItems: 'center',
        borderRadius: 10,
        borderColor: '#214457',
        borderWidth: 1,
        marginBottom: 24,
    },
    btnTxt: {
        color: 'white',
        fontSize: 16,
        fontWeight: '700',
        paddingVertical: 12
    },
    btnTxtGuest: {
        color: '#3E89AE',
        fontSize: 16,
        fontWeight: '700',
    },
    version: {
        fontSize: 12,
        fontWeight: '700',
        color: '#999999',
        position: 'absolute',
        bottom: 30
    }
})
