import React, { useState } from "react";
import { Alert, Modal, StyleSheet, Text, Pressable, View, FlatList, Dimensions } from "react-native";
import IonIcon from 'react-native-vector-icons/Ionicons'

import ButtonBlue from '../../components/ButtonBlue'

const ww = Dimensions.get('window').width
const wh = Dimensions.get('window').height

const ModalSort = ({ viewModal, modalType }) => {
    const [modalVisible, setModalVisible] = useState(false);
    const [isChecked, setIsChecked] = useState(false);

    const data = ['Today', 'Tomorrow', 'This Week', 'This Month', 'All Upcoming']

    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            setModalVisible(!modalVisible);
            }}
        >
            <View style={styles.modalContainer}>
                <View style={styles.modalView}>
                    <View style={styles.modalHeader}>
                        <IonIcon name={'close-outline'} size={ww * 0.08} color='black' />
                        <Text style={styles.modalText}>Test</Text>
                    </View>
                    <FlatList
                        data={data}
                        keyExtractor={(item, index) => index}
                        contentContainerStyle={{marginVertical: 12}}
                        renderItem={({item}) => (
                            <Pressable
                                style={{flexDirection: 'row', justifyContent: 'space-between', marginBottom: 12}}
                                onPress={() => alert(item)}
                            >
                                <Text style={[styles.textStyle, {color: isChecked ? '#214457' : 'black'}]}>{item}</Text>
                                {isChecked && <IonIcon name={'checkmark-outline'} size={ww * 0.07} color='#214457' />}
                            </Pressable>
                        )}
                    />
                    <ButtonBlue title='Apply' />
                </View>
            </View>
        </Modal>
    )
}

export default ModalSort

const styles = StyleSheet.create({
    modalContainer: {
        flex: 1,
        justifyContent: "flex-end",
        backgroundColor: 'rgba(153, 153, 153, 0.7)'
    },
    modalView: {
        backgroundColor: "white",
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        padding: 16,
        elevation: 5
    },
    modalHeader: {
        flexDirection: 'row',
        alignItems: 'center'
    },  
    textStyle: {
        fontSize: ww * 0.05,
    },
    modalText: {
        color: 'black',
        fontSize: ww * 0.06,
        fontWeight: '700',
        marginLeft: 10
    }
})
